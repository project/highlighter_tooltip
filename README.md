## About

This module adds simple highlighter or quote selectiion share link functionality to a website.

## Usage (not )

Connect the files `./dist/highlighter-tooltip.js` and `./dist/highlighter-tooltip.css` to your HTML.
The code is initialized with the following code after the DOM ready event, and attached to either the global `window` or `drupalSettings` objects.
```js
new HighlighterTooltipListener();
```
In Drupal, this is done for you. The code is connected to the `drupalSettings` object like so:
```js
drupalSettings.highlighterTooltipListener = new HighlighterTooltipListener({
  ...settings,
});
```

## Options

```js
new HighlighterTooltipListener({
  // The error message when the text fails to copy
  context: '#main', // OPTIONAL:  Defaults to document.body
  // The copy to clipboard message
  shareMessage = 'Share this highlight.', // OPTIONAL:  This is the default
  // The success message when the text is coppied
  successMessage = 'Copied to clipboard!',  // OPTIONAL:  This is the default
  // The error message when the text fails to copy
  errorMessage = 'Could not copy to clipboard!', // OPTIONAL:  This is the default
  // The URL for shortening URLs
  urlShortener: "/highlighter-tooltip/url-shortener" // OPTIONAL: The URL that returns a shortened version of the URL
});
```

## Development

Run `yarn` to install the repository.
Run `yarn dev` to work on files within `./src`.
