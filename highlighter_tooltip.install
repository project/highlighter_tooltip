<?php

/**
 * @file
 * Install, update and uninstall functions for the Highlighter Tooltip module.
 */

/**
 * Implements hook_install().
 */
function highlighter_tooltip_install() {
  $tables = highlighter_tooltip_schema(); // Your table schema definition as in Drupal Schema API.
  $database = \Drupal::database();
  $schema = $database->schema();
  foreach ($tables as $table_name => $table) {
    // First check if the tables exist and if missing...
    if (!$schema->tableExists($table_name)) {
      $schema->createTable($table_name, $table);
    }
  }
  \Drupal::messenger()->addStatus(__FUNCTION__);

}

/**
 * Implements hook_uninstall().
 */
function highlighter_tooltip_uninstall() {
  \Drupal::messenger()->addStatus(__FUNCTION__);
}

/**
 * Implements hook_schema().
 */
function highlighter_tooltip_schema() {
  $schema['highlighter_tooltip_urls'] = [
    'description' => 'Table description.',
    'fields' => [
      'id' => [
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique record ID.',
      ],
      'uid' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The {users}.uid of the user who created the record.',
      ],
      'status' => [
        'description' => 'Boolean indicating whether this record is active.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
      ],
      'created' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Timestamp when the record was created.',
      ],
      'path' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'size' => 'normal',
        'description' => 'The system path /node/x.',
      ],
      'alias' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'size' => 'normal',
        'description' => 'The alias of /node/x.',
      ],
      'long_hash' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'size' => 'normal',
        'description' => 'Highlighted text hash.',
      ],
      'short_hash' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'size' => 'normal',
        'description' => 'Shortened hash for highlighted text.',
      ],
    ],
    'primary key' => ['id'],
    'indexes' => [
      'uid' => ['uid'],
      'status' => ['status'],
    ],
  ];

  return $schema;
}

/**
 * Implements hook_requirements().
 */
function highlighter_tooltip_requirements($phase) {
  $requirements = [];

  if ($phase == 'runtime') {
    $value = mt_rand(0, 100);
    $requirements['highlighter_tooltip_status'] = [
      'title' => t('Highlighter Tooltip status'),
      'value' => t('Highlighter Tooltip value: @value', ['@value' => $value]),
      'severity' => $value > 50 ? REQUIREMENT_INFO : REQUIREMENT_WARNING,
    ];
  }

  return $requirements;
}

/**
 * Create new database table by update hook{custom_table}.
 */

  // function highlighter_tooltip_update_9001(){
  //   $database = \Drupal::database();
  //   $schema = $database->schema();
  //   $table_name = "highlighter_tooltip_urls";

  //   $table_schema = highlighter_tooltip_schema();

  //   if (!$schema->tableExists($table_name)) {
  //     $schema->createTable($table_name, $table);
  //   } 
  // }