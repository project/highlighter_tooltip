import HighlighterTooltipListener from './HighlighterTooltipListener';

document.addEventListener('DOMContentLoaded', (e) => {
  if (!!document.fragmentDirective) {
    const parentScope = window.drupalSettings || window;
    const settings = parentScope.highlighterTooltipSettings || {};
    parentScope.highlighterTooltipListener = new HighlighterTooltipListener({
      ...settings,
    });
  }
});
