<?php

namespace Drupal\highlighter_tooltip\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Highlighter Tooltip settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'highlighter_tooltip_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['highlighter_tooltip.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['share_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Share message'),
      '#default_value' => $this->config('highlighter_tooltip.settings')->get('share_message'),
      '#placeholder' => 'Share this highlight.'
    ];
    $form['success_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Success message'),
      '#default_value' => $this->config('highlighter_tooltip.settings')->get('success_message'),
      '#placeholder' => 'Copied to clipboard.'
    ];
    $form['error_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Error message'),
      '#default_value' => $this->config('highlighter_tooltip.settings')->get('error_message'),
      '#placeholder' => 'Oops. Something went wrong.'
    ];
    $form['context'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Context'),
      '#default_value' => $this->config('highlighter_tooltip.settings')->get('context'),
      '#placeholder' => "#main (a CSS/JS selector)"
    ];
    $form['custom_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom Domain'),
      '#default_value' => $this->config('highlighter_tooltip.settings')->get('custom_domain'),
      '#placeholder' => "http://example.com (optional)"
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();
    // check for URL scheme
    // TO DO: this accepts wonky scheme htps htts. fix.
    // if(!empty($values['custom_domain']) && preg_match('(http://|https://)', $values['custom_domain']) === 0)
    if (!empty($values['custom_domain']) && filter_var($values['custom_domain'], FILTER_VALIDATE_URL) === false)
    {
      $form_state->setErrorByName('custom_domain', $this->t('Enter a valid URL.'));
    }

    parent::validateForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('highlighter_tooltip.settings')
      ->set('share_message', $form_state->getValue('share_message'))
      ->set('success_message', $form_state->getValue('success_message'))
      ->set('error_message', $form_state->getValue('error_message'))
      ->set('context', $form_state->getValue('context'))
      ->set('custom_domain', $form_state->getValue('custom_domain'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
