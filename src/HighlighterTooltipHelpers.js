import { generateFragment } from 'text-fragments-polyfill/dist/fragment-generation-utils.js';

export const wordsOnEachSide = 3;

export const getClosestBlockElement = (node) => {
  const testNodeTagAsBlock = (el) => /^(address|blockquote|body|center|dir|div|dl|fieldset|form|h[1-6]|hr|isindex|menu|noframes|noscript|ol|p|pre|table|ul|dd|dt|frameset|li|tbody|td|tfoot|th|thead|tr|html)$/i.test(el.nodeName);

  if (testNodeTagAsBlock(node)) {
    return node;
  }
  else {
    let tmpNode = node;
    while (tmpNode.parentNode){
      tmpNode = tmpNode.parentNode
      if (testNodeTagAsBlock(tmpNode)) {
        return tmpNode;
      }
    }

    // Fallback
    return node;
  }
};

export const cleanTextForUrl = (text) => {
  return String(text)
    .trim()
    .replace(/,/g, '%2C')
    .replace(/\./g, '%2E')
    .replace(/\-/g, '%2D')
    .replace(/\!/g, '%21')
    .replace(/\&/g, '%26')
    .replace(/~/g, '%7E')
    .replace(/\(/g, '%28')
    .replace(/\)/g, '%29')
    .replace(/\n+/, ',')
    .replace(/\s+/g, '%20')
    .replace(/\'/g, '%27');
};

export const cleanTextForRegex = (text) => {
  return String(text)
    .replace(/\n+/, ' ') //Remove multi-lines
    .replace(/[^\w\s]$/, '') // Ending trailing punctuation (alt = /[^a-zA-Z0-9]$/)
    .replace(/^[^a-zA-Z0-9]/,'') // Starting punctuation
    // .replace(/\&/, '\\&')
    .replace(/\?/, '\\?')
    .replace(/\(/, '\\(') // Parenthesis fix
    .replace(/\)/, '\\)')
    // .replace(/\-/, '\\-')
    .trim();
  // .replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
};

export const checkWordEdges = (searchString, parentString) => {
  const cleanedSearchString = cleanTextForRegex(searchString);
  const cleanedParentString = cleanTextForRegex(parentString);

  const wordEdgeExp = "\(\[a-zA-Z0-9\.\?\!\]+|\\b|\\s\)";
  const expWordEdges = new RegExp([wordEdgeExp, cleanedSearchString, wordEdgeExp].join(''));
  const newSearchString = parentString.match(expWordEdges);

  // const searchIsEndOfParString = parentString.indexOf(searchString) + searchString.length == parentString.length;

  if (!!newSearchString) {
    return newSearchString[0].trim();
  }
  // Fallback, just return parent string
  return cleanedParentString.trim();
};

export const getElementByString = (string) => {
  return [...document.querySelectorAll('*')]
    .filter((element) => element.textContent?.match(cleanTextForRegex(string)))
    .pop();
};

export const makeHashUrl = (context, selection, textContainer) => {
  const text = selection.toString();
  let originalText = textContainer.innerText;
  let processedSearchText = String(text).trim();
  let processedText = String(text).trim();

  // If string long, lets use two short substrings
  if (processedText.split(' ').length > wordsOnEachSide * 2) {
    let startWords = processedText
      .split(' ')
      .slice(0, wordsOnEachSide)
      .join(' ');

    // If start words are over multiple lines, grab first item
    startWords = /\r|\n/.test(startWords)
      ? startWords.split(/\r|\n/).shift()
      : startWords;

    startWords = checkWordEdges(startWords, originalText);

    let endWords = processedText
      .split(' ')
      .slice(-wordsOnEachSide)
      .join(' ');

    // If end words are over multiple lines, grab last item
    endWords = /\r|\n/.test(endWords)
      ? endWords.split(/\r|\n/).pop()
      : endWords;

    // Double check that textContainer contains our end words
    // If it doesn't, find the element that does
    originalText =
      originalText.indexOf(endWords) != -1
        ? originalText
        : getElementByString(endWords)?.textContent;

    endWords = checkWordEdges(endWords, originalText);

    // Join those strings with a comma to show between
    processedText = [
      cleanTextForUrl(startWords),
      cleanTextForUrl(endWords),
    ].join(',');
  } else {
    processedText = checkWordEdges(processedText, originalText);
    processedText = cleanTextForUrl(processedText);
  }

  // Check if the context contains multiple times the same processedText
  const processedTextExp = new RegExp(processedSearchText, 'g');
  let processedTextCount = 0;
  if (processedTextExp) {
    processedTextCount = context.textContent.match(processedTextExp).length;
  } else {
      console.error("processedTextExp is null or undefined");
  }

  if (processedTextCount > 1) {
    // Remove the contextual links from the context to avoid
    // creating links that don't work when logged out.
    context.querySelectorAll('div[data-contextual-id]').forEach((element) => {
      element.remove();
    });
    return getUrlWithPrefixSuffix(context);
  }
  else {
    return `${window.location.href}#:~:text=${encodeURIComponent(processedText)}`;
  }
};

// Function to get a few words before and after the target text.
export const getUrlWithPrefixSuffix = () => {
  const selection = getSelection();
  const result = generateFragment(selection);

  if (result.status !== 0) {
    return null;
  }

  let url = `${location.origin}${location.pathname}${location.search}`;

  const fragment = result.fragment;
  const prefix = fragment.prefix
    ? `${cleanupPrefixSuffix(encodeURIComponent(cleanupSpaces(fragment.prefix)))}-,`
    : '';
  const suffix = fragment.suffix
    ? `,-${cleanupPrefixSuffix(encodeURIComponent(cleanupSpaces(fragment.suffix)))}`
    : '';
  const start = encodeURIComponent(fragment.textStart);
  const end = fragment. textEnd ? `,${encodeURIComponent(fragment. textEnd)}` : '';
  console.log(start, end);

  url += `#:~:text=${prefix}${start}${end}${suffix}`;

  return url;
};

export const cleanupSpaces = (value) => {
  // Remove double spaces to just one.
  // The library has some issues matching what the browser does.
  return String(value)
    .trim()
    .replace(/  +/g, ' ')
}

export const cleanupPrefixSuffix = (value) => {
  return String(value)
    .trim()
    .replace(/  +/g, ' ')
    .replace(/,/g, '%2C')
    .replace(/\-/g, '%2D')
    .replace(/\!/g, '%21')
    .replace(/\&/g, '%26')
    .replace(/~/g, '%7E')
    .replace(/\(/g, '%28')
    .replace(/\)/g, '%29')
    .replace(/\n+/, ',')
    .replace(/\'/g, '%27');
}
