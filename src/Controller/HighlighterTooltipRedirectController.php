<?php

namespace Drupal\highlighter_tooltip\Controller;

use Drupal\Core\Url;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Returns responses for Highlighter Tooltip routes.
 */
class HighlighterTooltipRedirectController extends ControllerBase {

  private const table_name = 'highlighter_tooltip_urls';
  private $database;

  private function setDb() {
    $this->database = \Drupal::database();
  }

  private function getUrlComponents($short_hash) {
    $db = $this->setDb();
    $database = $this->database;
    $table_name = HighlighterTooltipRedirectController::table_name;
    try {
      $query = $database->select('highlighter_tooltip_urls', 'htu')
        ->fields('htu',['path','alias','long_hash']) // return path, alias and long_hash
        ->condition('htu.short_hash', '/l/'.$short_hash) // match short_hash; TO DO: get route from declaration (see ajax controller)
        ->range(0,1);   // return 1 result
      $result = $query->execute();
      $records = $result->fetchAll();
      // return url components
      if (!$records) return null; 
      $url_components = [
        'path' => $records[0]->path,
        'alias'=> $records[0]->alias,
        'hash' => $records[0]->long_hash
      ];
      return $url_components;
    } catch (Exception $e) {
      // Log the exception to watchdog.
      \Drupal::logger('type')->error($e->getMessage());
    }

  }

  /**
   * Builds the response.
   * @param $id
   */
  public function urlRedirectFromHash($short_hash) {

    // get Url components of short hash
    $url_components = $this->getUrlComponents($short_hash);
    if ($url_components)
    {
      // get the node id by extracting from system path /node/xxxx
      // might be a better way to get the node id
      if ($url_components['path'] == '/'){ // if it's the homepage
        $url_components['path'] = \Drupal::configFactory()->get('system.site')->get('page.front');
      //   $node_path = explode('/',$url_components['path']);
      // } else  {  // if it's an aliased node
      //   $node_path = explode('/',$url_components['path']);
      } 

      $node_path = explode('/',$url_components['path']);
      // TO DO: verify if route_parameters still needed
      $route_parameters = isset($node_path[2]) ? ['node'=>$node_path[2]] : [];
      // get route name from given path
      $url_object = \Drupal::service('path.validator')->getUrlIfValid($url_components['path']);
      $route_name = $url_object->getRouteName();
      // build the destination URL
      $url = Url::fromRoute($route_name, $route_parameters, ['fragment' => $url_components['hash']]);

    } 

    // for debugging
    /* $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t(
        '<pre>hash: '.$short_hash.
        '<br/>url_components:'.print_r($url_components,1).
        //'<br/>route_name: '.$route_name.
        "<br/>node_path[]:".print_r($node_path,1).
        //"<br/>url_object: ".print_r($url_object,1).
        "<br/>url: ".print_r($url,1).
        "<br/>url->toString: ".print_r($url->toString(),1)."</pre>"
      ),
    ]; */

    // redirect to the page
    return new RedirectResponse($url->toString());
    // return $build;
  }

}
