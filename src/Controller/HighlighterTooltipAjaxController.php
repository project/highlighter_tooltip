<?php

namespace Drupal\highlighter_tooltip\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

class HighlighterTooltipAjaxController extends ControllerBase {

  private const table_name = 'highlighter_tooltip_urls';
  private $database;

  private function setDb() {
    $this->database = \Drupal::database();
  }

  /**
   * 
   * @param string $long_hash
   * 
   * @return string $short_hash
   */
  public function getShortUrl($long_hash) {
    // Query highlighter_tooltip_urls table for possible long_url match
    $db = $this->setDb();
    $database = $this->database;
    $table_name = HighlighterTooltipAjaxController::table_name;
    try {
      $query = $database->select('highlighter_tooltip_urls', 'htu')
        ->fields('htu',['short_hash']) // return short_url
        ->condition('htu.long_hash', $long_hash) // match long_url
        ->range(0,1);   // return 1 result
      $result = $query->execute();
      $records = $result->fetchAll();
      // return host + hash combo string
      if (!$records) return null; 
      $url = $records[0]->short_hash;
      return $url;
    } catch (Exception $e) {
      // Log the exception to watchdog.
      \Drupal::logger('type')->error($e->getMessage());
    }
  }

  public function setShortUrl($highlight_path, $highlight_fragment) {
    $db = $this->setDb();
    $database = $this->database;
    $table_name = HighlighterTooltipAjaxController::table_name;
    $alias_manager = \Drupal::service('path_alias.manager');

    // create an 8-char hash
    $hash_value = hash('crc32', $highlight_fragment); 
    $hash_prefix = "/l";  // TO DO: declare this somewhere else
    $hash_path = '/'.$hash_value;

    $txn = $database->startTransaction();
    try {
    // Save short + long urls to db
    $insert = $database->insert($table_name)
      ->fields([
        'path'=>$alias_manager->getPathByAlias($highlight_path),
        'alias'=>$highlight_path,
        'long_hash'=>$highlight_fragment,
        'short_hash'=>$hash_prefix.$hash_path,
        'created' => \Drupal::time()->getRequestTime(),
      ]);
    $insert->execute();
    // Return new hashed url
    $short_hash = $hash_prefix.$hash_path;
    return $short_hash;
    } catch (Exception $e) {
      // Something went wrong somewhere, so roll back now.
      $txn->rollBack();
      // Log the exception to watchdog.
      \Drupal::logger('type')->error($e->getMessage());
    }
  }

  /**
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function shortenUrl(): JsonResponse {
    $database = \Drupal::database();
    $host = \Drupal::request()->getSchemeAndHttpHost();
    $alias_manager = \Drupal::service('path_alias.manager');

    $ht_settings = \Drupal::config('highlighter_tooltip.settings');
    $custom_domain = $ht_settings->get('custom_domain');
    if ($custom_domain) $host = $custom_domain;
    // dump($custom_domain);

    // get contents of JsonResponse and decode it
    $json_content = \Drupal::request()->getContent();
    $decoded_content = json_decode($json_content, true);

    // parse the URL
    $highlight_url = $decoded_content['url'];
    $hu_components = parse_url($highlight_url);
    $highlight_path = $hu_components['path'];
    $highlight_fragment = $hu_components['fragment'];
    $table_name = HighlighterTooltipAjaxController::table_name;

    // If URL exists in table already, send back hashed existing URL
    $short_hash = $this->getShortUrl($highlight_fragment);
    if ($short_hash)
    {
        $url = $host.$short_hash;
    } 
    else
    // If no URL exists in table, send back new hashed URL
    {
      $url = $host.$this->setShortUrl($highlight_path, $highlight_fragment);
    }

    return new JsonResponse([
      'url' => $url
    ]);
  }

}
