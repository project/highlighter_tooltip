import HighlighterTooltip from './HighlighterTooltip';
import { getClosestBlockElement, makeHashUrl } from './HighlighterTooltipHelpers';

class HighlighterTooltipListener {

  tooltips = [];

  constructor(settings) {
    this.settings = settings;
    this.context = document.querySelector(this.settings.context) || document.body;
    document.addEventListener('mouseup', () => {
      const selection = window.getSelection();

      if (!!selection && selection.toString() != '') {
        // Loop up to nearest block element to get a long enough string
        // It will return the current element if it is a block
        const parentNode = getClosestBlockElement(selection.anchorNode?.parentNode);
        if (this.context.contains(parentNode)) {
          const url = makeHashUrl(this.context, selection, parentNode);

          this.createToolTip(url, parentNode);
        }
      }
    });
  }

  createToolTip(url, parentNode) {
    const tooltip = new HighlighterTooltip({
      ...this.settings,
      url,
      parentNode,
    });
    this.tooltips.push(tooltip);
  }
}

export default HighlighterTooltipListener;
