import copy from 'copy-to-clipboard';

class HighlighterTooltip extends HTMLElement {
  constructor({
    url,
    parentNode,
    shareMessage = 'Share this highlight.',
    successMessage = 'Copied to clipboard!',
    errorMessage = 'Could not copy to clipboard!',
    urlShortener = '',
  }) {
    super();

    this.parent = parentNode;
    this.url = url;
    this.urlShortener = urlShortener;
    this.messages = {
      share: shareMessage,
      success: successMessage,
      error: errorMessage,
    };

    this.init();
  }

  open = false;

  effectTime = 300;

  init() {
    this.textContent = this.messages.share;
    this.dataset.href = this.url;

    this.setPosition();

    this.show();

    this.addEventListener('click', (e) => {
      e.stopPropagation();

      this.classList.add('loading');
      this.copyUrlToClipboard(
        () => {
          this.textContent = this.messages.success;
          this.classList.remove('loading');
          this.hide();
        },
        () => {
          this.textContent = this.messages.error;
          this.classList.remove('loading');
          this.hide();
        })
    });

    // After open, set close event
    setTimeout(() => {
      document.body.addEventListener('click', (e) => {
        this.hide(0); //Immediatel hide
      });
    }, this.effectTime);

    // Reposition on window resize
    addEventListener('resize', () => {
      this.setPosition();
    });

    document.body.append(this);
  }

  copyUrlToClipboard(success = function() {}, error = function() {}) {
    const copyUrlToClipboard = (url) => {
      if (copy(url)) {
        success();
      }
      else {
        error();
      }
    };

    if (this.urlShortener != '') {
      fetch(this.urlShortener, {
        method: "POST",
        body: JSON.stringify({
          url: this.url,
        })
      })
        .then((e) => e.json())
        .then((e) => {
          // One last tier of feedback
          copyUrlToClipboard(e.url || this.url);
        }).catch((e) => {
          console.log("Shortened link not coppied to clipboard.", error);
          copyUrlToClipboard(this.url);
        });
    }
    else {
      copyUrlToClipboard(this.url);
    }
  }

  setPosition() {
    const parentPosition = this.getCoords();
    this.style.position = 'absolute'; //'fixed';
    this.style.cursor = 'pointer';
    this.style.top = `${parentPosition.top}px`;
    if (this.parent.getBoundingClientRect().right + 200 > window.innerWidth) {
      this.style.left = 'initial';
      this.style.right = `${20}px`;
    } else {
      this.style.right = 'initial';
      this.style.left = `${parentPosition.right + 20}px`;
    }
  }

  getCoords() {
    const parentRect = this.parent.getBoundingClientRect();
    return {
      top: parentRect.top + document.documentElement.scrollTop,
      left: parentRect.left + document.documentElement.scrollLeft,
      bottom:
        this.parent.offsetHeight +
        parentRect.top +
        document.documentElement.scrollTop,
      right:
        this.parent.offsetWidth +
        parentRect.left +
        document.documentElement.scrollLeft,
    };
  }

  show() {
    this.fade(true);
    this.open = true;
  }
  hide(effectTime = this.effectTime) {
    this.fade(false, effectTime);
    this.open = false;
  }

  fade(fadeIn = true, delay = 0) {
    setTimeout(() => {
      this.classList[fadeIn ? 'add' : 'remove']('active');
    }, delay);
  }
}

customElements.define('highlighter-tooltip', HighlighterTooltip);

export default HighlighterTooltip;
